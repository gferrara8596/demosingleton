//
//  CenterViewController.swift
//  demoSingleton
//
//  Created by Giuseppe Ferrara on 25/01/2020.
//  Copyright © 2020 Giuseppe Ferrara. All rights reserved.
//

import UIKit

class CenterViewController: UIViewController {
    @IBOutlet weak var leftSum: UILabel!
    @IBOutlet weak var totalSum: UILabel!
    @IBOutlet weak var rightSum: UILabel!
    
    let sumDatabase = SumDatabase()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        leftSum.text = String(sumDatabase.getLeftSum())
        rightSum.text = String(sumDatabase.getRightSum())
        totalSum.text = String(sumDatabase.getTotalSum())
    }

}

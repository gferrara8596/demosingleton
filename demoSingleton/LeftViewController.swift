//
//  LeftViewController.swift
//  demoSingleton
//
//  Created by Giuseppe Ferrara on 25/01/2020.
//  Copyright © 2020 Giuseppe Ferrara. All rights reserved.
//

import UIKit

class LeftViewController: UIViewController {

    @IBOutlet weak var sumLabel: UILabel!
    
    let sumDatabase = SumDatabase()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func addButtonPressed(_ sender: UIButton) {
        sumDatabase.addLeft()
        sumLabel.text = String(sumDatabase.getLeftSum())
    }
    
}

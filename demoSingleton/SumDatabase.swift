//
//  sumDatabase.swift
//  demoSingleton
//
//  Created by Giuseppe Ferrara on 25/01/2020.
//  Copyright © 2020 Giuseppe Ferrara. All rights reserved.
//

import Foundation

class SumDatabase {
    
    
    private var rightSum: Int = 0
    private var leftSum: Int = 0
    private var totalSum: Int = 0
    
    init(){
           
    }
       
    
    func addLeft(){
        leftSum = leftSum + 1
        totalSum = leftSum + rightSum
    }
    
    func addRight(){
        rightSum = rightSum + 1
        totalSum = leftSum + rightSum
    }
    
    func getLeftSum() -> Int {
        return leftSum
    }
    
    func getTotalSum() -> Int {
        return totalSum
    }
    
    
    func getRightSum() -> Int {
        return rightSum
    }
    
    
}
